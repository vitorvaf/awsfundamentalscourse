﻿using System.Text.Json;
using Amazon.SQS;
using Amazon.SQS.Model;
using SqsConsumer;

var cts = new CancellationTokenSource();

var sqsClient = new AmazonSQSClient();

var queueUrlResponse = await sqsClient.GetQueueUrlAsync("customers");

var receiveMessageRequest = new ReceiveMessageRequest
{
    QueueUrl = queueUrlResponse.QueueUrl
};

while(!cts.IsCancellationRequested)
{
    var response = await sqsClient.ReceiveMessageAsync(receiveMessageRequest, cts.Token);

    foreach (var message in response.Messages)
    {
        Console.WriteLine($"ID: { message.MessageId }");
        Console.WriteLine($"BODY: { message.Body } ");
        await sqsClient.DeleteMessageAsync(queueUrlResponse.QueueUrl, message.ReceiptHandle);        
    } 
    await Task.Delay(3000);

}
