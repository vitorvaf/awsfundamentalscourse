﻿using System.Text.Json;
using Amazon.SQS;
using Amazon.SQS.Model;
using SqsPublisher;

var sqsClient = new AmazonSQSClient();

var customer = new CustomerCreated
{
    Id = Guid.NewGuid(),
    Email = "vitorvaf@gmail.com",
    FullName = "Vitor de Abreu Freitas",
    DateOfBirth = new DateTime(1987, 7, 29),
    GitHubUserName = "vitorvaf"    
};

var queueUrlResponse = await sqsClient.GetQueueUrlAsync("customers");

var sendMensageRequest = new SendMessageRequest 
{
    QueueUrl = queueUrlResponse.QueueUrl,
    MessageBody = JsonSerializer.Serialize(customer),
    MessageAttributes = new Dictionary<string, MessageAttributeValue>
    {
        {
            "MessageType", new MessageAttributeValue
            {
                DataType = "String",
                StringValue = nameof(CustomerCreated)
            }
        }
    }
};

var response = await sqsClient.SendMessageAsync(sendMensageRequest);

Console.WriteLine();